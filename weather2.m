% Matlab R2020b script
% name: weather.m
% author: alekszy570
% date: 2020-11-25
% version: v2


%http://www.cumulus.nazwa.pl/teoria/laboratorium/l_tempodcz.htm
%Wzór na temperaturę odczuwalną (ang. wind chill) dla systemu metrycznego:
%Twc = 0.045 * (5.27**0.5 + 10.45 - 0.28 * V)*(T - 33) + 33
%gdzie: ** znaczy "do potęgi", V - prędkość wiatru w km/h, T - temperatura aktualna w st. C




%hour = clock;
%while hour(5) == 50

%download url
data_1 = urlread('https://danepubliczne.imgw.pl/api/data/synop/station/raciborz/format/json')
data_2 = urlread('https://danepubliczne.imgw.pl/api/data/synop/station/bielskobiala/format/json')
data_3 = urlread('https://danepubliczne.imgw.pl/api/data/synop/station/czestochowa/format/json')
data_4 = urlread('http://wttr.in/raciborz?format=j1')
data_5 = urlread('http://wttr.in/bielsko-biala?format=j1')
data_6 = urlread('http://wttr.in/czestochowa?format=j1')

%decode data
survey_1 = jsondecode(data_1)
survey_2 = jsondecode(data_2)
survey_3 = jsondecode(data_3)
survey_4 = jsondecode(data_4)
survey_5 = jsondecode(data_5)
survey_6 = jsondecode(data_6)

%calculate sensed temperature
sensed_temperature_1 = 0.045*(5.27^0.5+10.45-0.28*survey_1.predkosc_wiatru)*(survey_1.temperatura-33)+33
sensed_temperature_2 = 0.045*(5.27^0.5+10.45-0.28*survey_2.predkosc_wiatru)*(survey_2.temperatura-33)+33
sensed_temperature_3 = 0.045*(5.27^0.5+10.45-0.28*survey_3.predkosc_wiatru)*(survey_3.temperatura-33)+33

%create tabble for data
A = {'station', 'date', 'date_hour', 'temperature', 'pressure', 'windspeedh', 'humidity', 'sensed_temperature';
survey_1.stacja, survey_1.data_pomiaru, survey_1.godzina_pomiaru, survey_1.temperatura, survey_1.cisnienie, survey_1.predkosc_wiatru, survey_1.wilgotnosc_wzgledna, sensed_temperature_1;
survey_2.stacja, survey_2.data_pomiaru, survey_2.godzina_pomiaru, survey_2.temperatura, survey_2.cisnienie, survey_2.predkosc_wiatru, survey_2.wilgotnosc_wzgledna, sensed_temperature_2;
survey_3.stacja, survey_3.data_pomiaru, survey_3.godzina_pomiaru, survey_3.temperatura, survey_3.cisnienie, survey_3.predkosc_wiatru, survey_3.wilgotnosc_wzgledna, sensed_temperature_3;
'Racibórz', 'dane', survey_4.current_condition.localObsDateTime, survey_4.current_condition.temp_C, survey_4.current_condition.pressure, survey_4.current_condition.windspeedKmph, survey_4.current_condition.humidity, survey_4.current_condition.FeelsLikeC;
'Bielsko_Biala', 'dane', survey_5.current_condition.localObsDateTime, survey_5.current_condition.temp_C, survey_5.current_condition.pressure, survey_5.current_condition.windspeedKmph, survey_5.current_condition.humidity, survey_5.current_condition.FeelsLikeC;
'Czestochowa', 'dane', survey_6.current_condition.localObsDateTime, survey_6.current_condition.temp_C, survey_6.current_condition.pressure, survey_6.current_condition.windspeedKmph, survey_6.current_condition.humidity, survey_6.current_condition.FeelsLikeC};

%change array to table
A_1 = array2table(A)

%write table to weatherData.csv
writetable(A_1, 'C:\Users\48664\Desktop\matlab\weatherData.csv')