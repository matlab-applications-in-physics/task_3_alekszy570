% Matlab R2020b script
% name: weather.m
% author: alekszy570
% date: 2020-11-25
% version: v1


%http://www.cumulus.nazwa.pl/teoria/laboratorium/l_tempodcz.htm
%Wzór na temperaturę odczuwalną (ang. wind chill) dla systemu metrycznego:
%Twc = 0.045 * (5.27**0.5 + 10.45 - 0.28 * V)*(T - 33) + 33
%gdzie: ** znaczy "do potęgi", V - prędkość wiatru w km/h, T - temperatura aktualna w st. C


Zapisywać jako csv

data_1 = urlread('https://danepubliczne.imgw.pl/api/data/synop/station/raciborz/format/json')
data_2 = urlread('https://danepubliczne.imgw.pl/api/data/synop/station/bielskobiala/format/json')
data_3 = urlread('https://danepubliczne.imgw.pl/api/data/synop/station/czestochowa/format/json')
data_4 = urlread('http://wttr.in/raciborz?format=j1')
data_5 = urlread('http://wttr.in/bielsko-biala?format=j1')
data_6 = urlread('http://wttr.in/czestochowa?format=j1')

survey_1 = jsondecode(data_1)
survey_2 = jsondecode(data_2)
survey_3 = jsondecode(data_3)
survey_4 = jsondecode(data_4)
survey_5 = jsondecode(data_5)
survey_6 = jsondecode(data_6)

station_1 = survey_1.stacja
date_1 = survey_1.data_pomiaru
hour_1 = survey_1.godzina_pomiaru
temperature_1 = survey_1.temperatura
pressure_1 = survey_1.cisnienie
wind_speed_1 = survey_1.predkosc_wiatru
moisture_1 = survey_1.wilgotnosc_wzgledna
sensed_temperature_1 = 0.045*(5.27^0.5+10.45-0.28*survey_1.predkosc_wiatru)*(survey_1.temperatura-33)+33


station_2 = survey_2.stacja
date_2 = survey_2.data_pomiaru
hour_2 = survey_2.godzina_pomiaru
temperature_2 = survey_2.temperatura
pressure_2 = survey_2.cisnienie
wind_speed_2 = survey_2.predkosc_wiatru
moisture_2 = survey_2.wilgotnosc_wzgledna
sensed_temperature_2 = 0.045*(5.27^0.5+10.45-0.28*survey_2.predkosc_wiatru)*(survey_2.temperatura-33)+33

station_3 = survey_3.stacja
date_3 = survey_3.data_pomiaru
hour_3 = survey_3.godzina_pomiaru
temperature_3 = survey_3.temperatura
pressure_3 = survey_3.cisnienie
wind_speed_3 = survey_3.predkosc_wiatru
moisture_3 = survey_3.wilgotnosc_wzgledna
sensed_temperature_3 = 0.045*(5.27^0.5+10.45-0.28*survey_3.predkosc_wiatru)*(survey_3.temperatura-33)+33

hour = clock;
while hour(5) == 50

A(1,1) = {'Raciborz'};
A(1,2) = {'Data'};
A(1,3) = {'Godzina'};
A(1,4) = {'Temperatura'};
A(1,5) = {'Cisnienie'};
A(1,6) = {'Predkosc wiatru'};
A(1,7) = {'Wilgotnosc wzgledna'};
A(1,8) = {'Temperatura odczuwalna'}
A(1,9) = {'Bielsko Biala'};
A(1,10) = {'Data'};
A(1,11) = {'Godzina'};
A(1,12) = {'Temperatura'};
A(1,13) = {'Cisnienie'};
A(1,14) = {'Predkosc wiatru'};
A(1,15) = {'Wilgotnosc wzgledna'};
A(1,16) = {'Temperatura odczuwalna'}
A(1,17) = {'Czestochowa'};
A(1,18) = {'Data'};
A(1,19) = {'Godzina'};
A(1,20) = {'Temperatura'};
A(1,21) = {'Cisnienie'};
A(1,22) = {'Predkosc wiatru'};
A(1,23) = {'Wilgotnosc wzgledna'};
A(1,24) = {'Temperatura odczuwalna'}



B = {survey_1.stacja, survey_1.data_pomiaru, survey_1.godzina_pomiaru, survey_1.temperatura, survey_1.cisnienie, survey_1.predkosc_wiatru, survey_1.wilgotnosc_wzgledna, 10}

survey_2.stacja, survey_2.data_pomiaru, survey_2.godzina_pomiaru, survey_2.temperatura, survey_2.cisnienie, survey_2.predkosc_wiatru, survey_2.wilgotnosc_wzgledna, 10, survey_3.stacja, survey_3.data_pomiaru,
survey_3.godzina_pomiaru, survey_3.temperatura, survey_3.cisnienie, survey_3.predkosc_wiatru, survey_3.wilgotnosc_wzgledna, 10};

C = [A; B]


weather = 'C:\Users\48664\Desktop\matlab\weatherData.csv'
weather_open = fopen('weather');
dlmwrite('weather_open',A,char)


 


